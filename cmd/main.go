package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/delivery-go-react/back/upload-svc/pkg/config"
	"gitlab.com/delivery-go-react/back/upload-svc/pkg/db"
	"gitlab.com/delivery-go-react/back/upload-svc/pkg/pb"
	"gitlab.com/delivery-go-react/back/upload-svc/pkg/services"
	"google.golang.org/grpc"
)

func main() {
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	h := db.Init(c)

	lis, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to listing:", err)
	}

	fmt.Println("Role Svc on", c.Port)

	s := services.Server{
		H: h,
		C: c,
	}

	grpcServer := grpc.NewServer()

	pb.RegisterUploadServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalln("Failed to serve:", err)
	}
}
