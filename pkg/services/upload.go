package services

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"cloud.google.com/go/storage"
	"google.golang.org/api/option"

	"gitlab.com/delivery-go-react/back/upload-svc/pkg/config"
	"gitlab.com/delivery-go-react/back/upload-svc/pkg/db"
	"gitlab.com/delivery-go-react/back/upload-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/upload-svc/pkg/pb"
)

type Server struct {
	H db.Handler
	C config.Config
}

func getCredentialsPath() (string, error) {
	wd, err := os.Getwd()
	if err != nil {
		return "", err
	}
	return filepath.Join(wd, "credentials", "rock-elevator-382907-c9b282470dbe.json"), nil
}

func (s *Server) UploadFile(ctx context.Context, req *pb.UploadFileRequest) (*pb.UploadFileResponse, error) {
	credentialsPath, err := getCredentialsPath()
	if err != nil {
		return nil, fmt.Errorf("failed to get credentials path: %v", err)
	}

	storageClient, err := storage.NewClient(ctx, option.WithCredentialsFile(credentialsPath))
	if err != nil {
		return nil, fmt.Errorf("failed to create storage client: %v", err)
	}
	defer storageClient.Close()

	bucketName := "delivery-go-react-bucket"
	bucket := storageClient.Bucket(bucketName)

	filename := fmt.Sprintf("%s-%d", req.Filename, time.Now().UnixNano())

	object := bucket.Object(filename)
	writer := object.NewWriter(ctx)

	contentType := http.DetectContentType(req.Content)

	writer.ContentType = contentType

	if _, err := io.Copy(writer, bytes.NewReader(req.Content)); err != nil {
		return nil, fmt.Errorf("failed to upload file: %v", err)
	}

	if err := writer.Close(); err != nil {
		return nil, fmt.Errorf("failed to close writer: %v", err)
	}

	url := fmt.Sprintf("https://storage.googleapis.com/%s/%s", bucketName, filename)

	fileRecord := &models.File{
		Name: req.Filename,
		Link: url,
		Type: contentType,
	}

	if err := s.H.DB.Create(&fileRecord).Error; err != nil {
		return nil, fmt.Errorf("failed to save file information to the database: %v", err)
	}

	return &pb.UploadFileResponse{Url: url}, nil
}

func (s *Server) GetUploadFile(ctx context.Context, req *pb.GetFileRequest) (*pb.GetFileResponse, error) {
	file := &models.File{}
	if result := s.H.DB.First(&file, req.Id); result.Error != nil {
		return nil, result.Error
	}

	response := &pb.GetFileResponse{
		Url: file.Link,
	}
	return response, nil
}
