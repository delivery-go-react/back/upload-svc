package config

import "github.com/spf13/viper"

type Config struct {
	Port              string `mapstructure:"PORT"`
	DB_USER           string `mapstructure:"DB_USER"`
	DB_PASSWORD       string `mapstructure:"DB_PASSWORD"`
	DB_HOST           string `mapstructure:"DB_HOST"`
	DB_NAME           string `mapstructure:"DB_NAME"`
	GoogleCredentials string `mapstructure:"GOOGLE_BUCKET_CRED"`
}

func LoadConfig() (config Config, err error) {
	viper.AddConfigPath("./pkg/config/envs")
	viper.SetConfigName("dev")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()

	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)

	return
}
