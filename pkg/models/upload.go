package models

type File struct {
	ID   int64  `gorm:"primaryKey"`
	Name string `gorm:"not null"`
	Link string `gorm:"not null"`
	Type string `gorm:"not null"`
}
